import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

interface IRenderDTO {
  img_path: string;
  audio_path?: string;
  template_path: string;
  text: string;
  description: string;
  subtitle: string;
  textColor: string;
}

@Injectable({
  providedIn: 'root',
})
export class UploadAepService {
  settings = {
    skipCleanup: true,
    addLicense: false,
    debug: true,
  };

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'nexrender-secret': 'myapisecret',
    }),
  };

  constructor(private http: HttpClient) {}

  async uploadAepFile(file: File) {
    const formData = new FormData();
    formData.append('projectFile', file);

    const httpHeader = {
      headers: new HttpHeaders({
        'nexrender-secret': 'myapisecret',
      }),
    };

    return await this.http
      .post('http://localhost:3000/render/upload', formData, httpHeader)
      .toPromise();
  }

  async renderizar({
    audio_path,
    template_path,
    img_path,
    textColor,
    text,
    description,
    subtitle,
  }: IRenderDTO) {
    const job = {
      template: {
        src: `file:///${template_path}`,
        composition: 'main',
        outputExt: 'avi',
        output: 'avi',
      },
      assets: [
        {
          src: img_path,
          type: 'image',
          layerName: 'background',
        },
        {
          src: audio_path,
          type: 'audio',
          layerName: 'soundtrack',
        },
        {
          type: 'data',
          layerName: 'text',
          property: 'Source Text',
          value: `${text}`,
        },
        {
          type: 'data',
          layerName: 'text',
          property: 'Source Text.font',
          value: 'Arial-BoldItalicMT',
        },
        {
          type: 'data',
          layerName: 'text',
          property: 'Source Text.fontSize',
          value: 38,
        },
        {
          type: 'data',
          layerName: 'text',
          property: 'Source Text.fillColor',
          value:
            textColor == 'r'
              ? [1, 0, 0]
              : textColor == 'g'
              ? [0, 1, 0]
              : [0, 0, 1],
        },
        {
          type: 'data',
          layerName: 'subtitle',
          property: 'Source Text',
          value: `${subtitle}`,
        },
        {
          type: 'data',
          layerName: 'subtitle',
          property: 'Source Text.font',
          value: 'Arial-BoldItalicMT',
        },
        {
          type: 'data',
          layerName: 'subtitle',
          property: 'Source Text.fontSize',
          value: 38,
        },
        {
          type: 'data',
          layerName: 'subtitle',
          property: 'Source Text.fillColor',
          value:
            textColor == 'r'
              ? [1, 0, 0]
              : textColor == 'g'
              ? [0, 1, 0]
              : [0, 0, 1],
        },
        {
          type: 'data',
          layerName: 'description',
          property: 'Source Text',
          value: `${description}`,
        },
        {
          type: 'data',
          layerName: 'description',
          property: 'Source Text.font',
          value: 'Arial-BoldItalicMT',
        },
        {
          type: 'data',
          layerName: 'description',
          property: 'Source Text.fontSize',
          value: 38,
        },
        {
          type: 'data',
          layerName: 'description',
          property: 'Source Text.fillColor',
          value:
            textColor == 'r'
              ? [1, 0, 0]
              : textColor == 'g'
              ? [0, 1, 0]
              : [0, 0, 1],
        },
      ],
      actions: {
        postrender: [
          {
            module: '@nexrender/action-encode',
            preset: 'mp4',
            output: 'result.mp4',
            params: { '-vcodec': 'libx264', '-r': 25 },
          },
        ],
      },
    };

    console.log(job);

    return this.http
      .post('http://localhost:3000/render/api/v1/jobs', job, this.httpOptions)
      .toPromise();
  }
}
