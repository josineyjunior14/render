import { TestBed } from '@angular/core/testing';

import { UploadAepService } from './upload-aep.service';

describe('UploadAepService', () => {
  let service: UploadAepService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(UploadAepService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
