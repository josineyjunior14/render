import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class DownloadService {
  constructor(private http: HttpClient) {}

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'nexrender-secret': 'myapisecret',
    }),
  };

  async downloadFile(file: string) {
    console.log('entrou');

    return await this.http
      .get(`http://localhost:3000/static/5JDeod73guDhGuWW_zJC2/img2.jpg`, this.httpOptions)
      .toPromise();
  }
}
