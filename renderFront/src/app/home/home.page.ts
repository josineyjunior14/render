import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoadingService } from '../services/loading.service';
import { MessageService } from '../services/message.service';
import { UploadAepService } from './service/upload-aep.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  uploadForm = new FormGroup({
    musica: new FormControl(''),
    imagem: new FormControl(''),
    arquivo: new FormControl(''),
    texto: new FormControl('', Validators.required),
    cor: new FormControl(''),
  });

  uploadedTemplateFile: any;
  uploadedCsvFile: any;
  selectedFile: File;

  musicas: Array<any> = [
    {
      absolutePath:
        'file:///C:/Users/josin/Documents/renderization/renderFront/src/assets/examples/audio/audio1.mp3',
      path: '../../../assets/examples/audio/audio1.ogg',
      value: 0,
      checked: false,
    },
    {
      absolutePath:
        'file:///C:/Users/josin/Documents/renderization/renderFront/src/assets/examples/audio/audio2.mp3',
      path: '../../../assets/examples/audio/audio2.ogg',
      value: 1,
      checked: false,
    },
  ];

  imagens: Array<any> = [
    {
      absolutePath:
        'file:///C:/Users/josin/Documents/renderization/renderFront/src/assets/examples/image/img1.jpg',
      path: '../../../assets/examples/image/img1.jpg',
      value: 0,
      checked: false,
    },
    {
      absolutePath:
        'file:///C:/Users/josin/Documents/renderization/renderFront/src/assets/examples/image/img2.jpg',
      path: '../../../assets/examples/image/img2.jpg',
      value: 1,
      checked: false,
    },
  ];

  constructor(
    private renderizar: UploadAepService,
    private loadingService: LoadingService,
    private messageService: MessageService
  ) {}

  ngOnInit() {}

  onChangeUpload(event: any) {
    this.selectedFile = <File>event.srcElement.files[0];
    document.getElementById('customFileLabel').innerText =
      this.selectedFile.name;
  }

  onRadioChangeMusica(event) {
    const formControl: FormControl = this.uploadForm.get(
      'musica'
    ) as FormControl;

    if (event.target.attributes['aria-checked'].value) {
      formControl.setValue(event.target.attributes['ng-reflect-value'].value);
    }
    //else {
    //   console.log(event.target.attributes['aria-checked'].value);
    // }

    console.log('[musica] -> ', this.uploadForm.get('musica').value);
  }

  onRadioChangeImagem(event) {
    const formControl: FormControl = this.uploadForm.get(
      'imagem'
    ) as FormControl;

    if (event.target.attributes['aria-checked'].value) {
      formControl.setValue(event.target.attributes['ng-reflect-value'].value);
    }

    console.log('[imagem] -> ', this.uploadForm.get('imagem').value);
  }

  async onUpload(file: File) {
    // console.log(file);

    this.loadingService.present('Uploading arquivo...');
    try {
      const result: any = await this.renderizar.uploadAepFile(file);
      if (result.rows) {
        this.uploadedCsvFile = result;
        console.log(this.uploadedCsvFile.rows[0], 'aqui');
      } else {
        this.uploadedTemplateFile = result;
        console.log(this.uploadedTemplateFile);
      }
      this.loadingService.dismiss();
    } catch (error) {
      this.loadingService.dismiss();
      this.messageService.exibirMensagem(error.message);
    }
  }

  async onSubmit() {
    // console.log(this.uploadForm.get('cor').value);

    this.loadingService.present('Criando job...');
    if (this.uploadedCsvFile && this.uploadedTemplateFile) {
      try {
        let jobs: any[] = [];
        for (const row of this.uploadedCsvFile.rows) {
          const result = this.renderizar
            .renderizar({
              template_path: this.uploadedTemplateFile.path
                .replaceAll('\\', '/')
                .replace('C', 'c'),
              img_path: row.img,
              text: row.text,
              description: row.description,
              subtitle: row.subtitle,
              textColor: 'r',
              audio_path:
                this.musicas[this.uploadForm.get('musica').value].absolutePath,
            })
            .then((job) => {
              this.loadingService.dismiss();
              jobs.push(job);
            });
        }

        return jobs;
      } catch (error) {
        this.loadingService.dismiss();
        this.messageService.exibirMensagem(error.message);
      }
    }
  }
}
