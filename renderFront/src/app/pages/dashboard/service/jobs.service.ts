import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class JobsService {
  API = 'http://localhost:3000/render/api/v1';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'nexrender-secret': 'myapisecret',
    }),
  };

  constructor(private http: HttpClient) {}

  async getAllJobs() {
    let auxUrl = this.API + '/jobs';
    return await this.http.get(auxUrl, this.httpOptions).toPromise();
  }

  async deleteJob(uid: string) {
    let auxUrl = this.API + '/jobs/';
    return await this.http
      .delete(`${auxUrl}${uid}`, this.httpOptions)
      .toPromise();
  }
}
