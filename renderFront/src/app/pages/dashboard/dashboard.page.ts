import { Component, OnInit } from '@angular/core';
import { JobsService } from './service/jobs.service';
import { interval } from 'rxjs';
import { DownloadService } from 'src/app/home/service/download.service';
import { LoadingService } from 'src/app/services/loading.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {
  jobs: any = [];

  fileToUpload: File = null;

  source = interval(2000);

  constructor(
    private jobsService: JobsService,
    private downloadService: DownloadService,
    private loadingService: LoadingService
  ) {}

  ngOnInit() {
    //output: 0,1,2,3,4,5...
    this.listenerJobs();
  }

  listenerJobs() {
    this.source.subscribe((val) =>
      this.jobsService.getAllJobs().then((results) => {
        this.jobs = results;
        for (const job of this.jobs) {
          const splitedOutput = job.output.split('\\');
          job.downloadAvi =
            splitedOutput[splitedOutput.length - 2] +
            '/' +
            splitedOutput[splitedOutput.length - 1];
          // console.log(job.downloadAvi);
          job.downloadMp4 = job.downloadAvi;
          job.downloadMp4 = job.downloadMp4.replace('.avi', '.mp4');
          // console.log(job.downloadMp4, 'aqui', job.downloadAvi);
        }
      })
    );
  }

  test() {
    this.jobs = [];
  }

  toStringProgress(progress) {
    return progress.toString();
  }

  async downloadFile(file: string) {
    console.log(await this.downloadService.downloadFile(file), 'w');
  }

  async deleteJob(uid: string) {
    const result = await this.jobsService.deleteJob(uid);
    console.log(result);

    return result;
  }
}
