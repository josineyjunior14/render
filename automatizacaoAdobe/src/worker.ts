import { start } from "@nexrender/worker";

 async function main(): Promise<void> {
  const serverHost = "http://localhost:3000/render";
  const serverSecret = "myapisecret";

  await start(serverHost, serverSecret, {
    workpath: "src/saida",
    skipCleanup: true,
    addLicense: false,
    debug: true,
  });
};

main().catch(console.error);
