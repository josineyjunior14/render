import "reflect-metadata";

import express from "express";
import "express-async-errors";
import path from 'path'

import renderRoutes from "./routes/render";

const app = express();
const port = 3000;

app.use("/static",express.static(path.join(__dirname, '..', '..', '..', '..', '..', 'automatizacaoAdobe', 'src', 'saida')));

app.use("/render", renderRoutes);
// app.use("/upload", uploadRoute);
// app.use("/download", downloadRoute);

app.listen(port, () => {
  console.log(`🔥 Server started on port ${port} ${path.join(__dirname, '..', '..', '..', '..', '..', 'automatizacaoAdobe', 'saida')}`);
});
