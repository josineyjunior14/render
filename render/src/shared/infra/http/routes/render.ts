import express, { Router } from "express";
import cors from "cors";

import { createHandler } from "@nexrender/server";
import downloadRoute from "../routes/donwload";
import uploadRoute from "../routes/upload";

const handler = createHandler("myapisecret");

const renderRoutes = Router();

renderRoutes.use(cors());
renderRoutes.use("/download", downloadRoute);
renderRoutes.use("/upload", uploadRoute);
renderRoutes.use(handler);

export default renderRoutes;
