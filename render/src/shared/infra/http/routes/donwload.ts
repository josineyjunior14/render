import express, { Request, Response, response, Router } from "express";
import path from "path";
import fs from "fs";
import mime from "mime";

const downloadRoute = Router();

downloadRoute.use(express.json());
downloadRoute.get("/", (request: Request, response: Response) => {
  const file =
    "C:/Users/josin/Documents/renderFront/src/assets/examples/audio/audio1.mp3";

  const filename = path.basename(file);
  // const mimetype = mime.lookup(file);

  response.setHeader("Content-disposition", "attachment; filename=" + filename);
  // response.setHeader("Content-type", mimetype);

  var filestream = fs.createReadStream(file);
  response.download(file, 'teste.avi');
});

export default downloadRoute;
