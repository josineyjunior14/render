import { Request, Response, response, Router } from "express";
import multer from "multer";
import * as csv from "csv-parser";
import fs from "fs";

import uploadConfig from "../../../../config/upload";

const uploadRoute = Router();

const upload = multer(uploadConfig);

uploadRoute.post(
  "/",
  upload.single("projectFile"),
  async (request: Request, response: Response) => {
    const { file } = request;

    const splitedOriginalFileName = file.originalname.split(".");

    if (splitedOriginalFileName[splitedOriginalFileName.length - 1] === "csv") {
      file.rows = [];

      fs.createReadStream(file.path)
        .pipe(csv.default())
        .on("data", (row) => {
          file.rows.push(row);
        })
        .on("end", () => {
          return response.json(file);
        });
    } else {
      return response.json(file);
    }
  }
);

export default uploadRoute;
